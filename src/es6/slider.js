const sliderData = [
    {
        image: "./src/img/03.jpg",
        title: "Mountain stream"
    },
    {
        image: "./src/img/04.jpg",
        title: "Mountains"
    },
    {
        image: "./src/img/06.jpg",
        title: "Red mountains"
    },
    {
        image: "./src/img/jq.png",
        title: "jq))"
    }
];

function generateSliderTNS(container) {
    return ({
        "container": container,
        "items": 1,
        "slideBy": "page",
        "loop": false,
        "startIndex": 0,
        "swipeAngle": false,
        "speed": 400,
        "controlsText": ['', '']
    });
}

function* generateSlide(sliderData) {
    for (let i = 0; i < sliderData.length; i++) {
        yield sliderData[i];
    }
}

function createImgToSlider(imgBox, slide) {
    
    const img = document.createElement('img');
    img.setAttribute('src', slide.value.image);
    imgBox.append(img);

    const span = document.createElement('span');
    span.classList.add('slide-text');
    span.innerHTML = slide.value.title;
    imgBox.append(span);

    imgBox.classList.remove('empty-slide');
}

function markUpMySlider() {
    const mySlider = document.querySelector('.my-slider');
    
    sliderData.forEach(() => {
        const div = document.createElement('div');
        div.classList.add('empty-slide');
        return mySlider.append(div);
    });
}

export { generateSliderTNS, sliderData, generateSlide, createImgToSlider, markUpMySlider };