import '@babel/polyfill';
import '../scss/main.scss';    //мои стили
import '../../node_modules/tiny-slider/src/tiny-slider.scss';
import { tns } from '../../node_modules/tiny-slider/src/tiny-slider.js';
import { generateSliderTNS } from './slider';   //"генератор" объекта настроек слайдера
import { sliderData } from './slider';      //наши картинки и тд
import { generateSlide } from './slider';   //ф-ция генератор !!!
import { createImgToSlider } from './slider';   //добавляет картинку в слайдер
import { markUpMySlider } from './slider';      //разметка слайдера

document.addEventListener('DOMContentLoaded', function() {

    markUpMySlider();
    
    const slide = generateSlide(sliderData);
    const slider = tns(generateSliderTNS('.my-slider'));
    const btnNext = slider.getInfo().nextButton;
    const navContainer = slider.getInfo().navContainer;

    function isGeneratorDone(slide) {
        if ( !slide.done ) {
            navContainer.hidden = true;
            const emptySlide = document.querySelector('.empty-slide');
            createImgToSlider(emptySlide, slide);
        } else {
            navContainer.hidden = false;
        }
    }

    isGeneratorDone(slide.next());
    btnNext.addEventListener('click', () => isGeneratorDone(slide.next()));
})
